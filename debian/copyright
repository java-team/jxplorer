Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JXplorer - A Java Ldap Browser
Upstream-Contact: Chris Betts <jxplorer@pegacat.com>
Source: https://sourceforge.net/projects/jxplorer/

Files: *
Copyright: 2002-2010, Computer Associates
License: custom-license
 Computer Associates Open Source Software License V.1.0
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 3. The end-user documentation included with the redistribution, if any,
 must include the following acknowledgment:
 "This product includes software developed by Computer Associates
 (http://www.ca.com/)."
 Alternately, this acknowledgment may appear in the software itself, if
 and wherever such third-party acknowledgments normally appear.
 4. The name "Computer Associates" must not be used to endorse or promote
 products derived from this software without prior written permission.
 5. Products may not include "Computer Associates" their name, without prior
 written permission of the Computer Associates.
 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 COMPUTER ASSOCIATES OR CONTRIBUTORS TO THE JXPLORER OPEN SOURCE PROJECT
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: src/org/apache/axis/utils/*
Copyright: 2001, The Apache Software Foundation
License: Apache-1.1
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with the
 distribution.
 3. The end-user documentation included with the redistribution,
 if any, must include the following acknowledgment:
 "This product includes software developed by the
 Apache Software Foundation (http://www.apache.org/)."
 Alternately, this acknowledgment may appear in the software itself,
 if and wherever such third-party acknowledgments normally appear.
 4. The names "Apache" and "Apache Software Foundation" must
 not be used to endorse or promote products derived from this
 software without prior written permission. For written
 permission, please contact apache@apache.org.
 5. Products derived from this software may not be called "Apache",
 nor may "Apache" appear in their name, without prior written
 permission of the Apache Software Foundation.
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 This software consists of voluntary contributions made by many
 individuals on behalf of the Apache Software Foundation.  For more
 information on the Apache Software Foundation, please see
 <http://www.apache.org/>.
 Portions of this software are based upon public domain software
 originally written at the National Center for Supercomputing Applications,
 University of Illinois, Urbana-Champaign.

Files: debian/*
Copyright: 2010, Gabriele Giacone <1o5g4r8o@gmail.com>
           2017, tony mancill <tmancill@debian.org>
License: GPL-3+
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-3'
 file.
