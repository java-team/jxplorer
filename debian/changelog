jxplorer (3.3.2+dfsg-7) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 21 (Closes: #1053043)
  * Standards-Version updated to 4.6.2
  * No longer build the javadoc

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 21 Nov 2023 08:44:06 +0100

jxplorer (3.3.2+dfsg-6) unstable; urgency=medium

  * Update Vcs fields for migration from Alioth -> Salsa
  * Bump Standards-Version to 4.3.0
  * Drop needless runtime dependency upon junit

 -- tony mancill <tmancill@debian.org>  Sun, 13 Jan 2019 17:43:07 -0800

jxplorer (3.3.2+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 9 (Closes: #875591)
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 08 Mar 2018 00:36:27 +0100

jxplorer (3.3.2+dfsg-4) unstable; urgency=medium

  * Upload to unstable.
  * Bump Standards-Version to 4.0.0

 -- tony mancill <tmancill@debian.org>  Sat, 24 Jun 2017 16:41:36 -0700

jxplorer (3.3.2+dfsg-3) experimental; urgency=medium

  [ Emmanuel Bourg ]
  * debian/copyright: Fixed the short name of the Apache-1.1 license

  [ tony mancill ]
  * Prevent embedding of timestamps in icons to make the build
    reproducible. (Closes: #776758)
    - Thank you to Reiner Herrmann for the bug report and patch.
  * Remove Gabriele Giacone from Uploaders (Closes: #856763)
  * Bump Standards-Version to 3.9.8.
  * Use debhelper 10.
  * Freshen debian/copyright.
  * Remove menu file (see #741573).

 -- tony mancill <tmancill@debian.org>  Mon, 20 Mar 2017 21:22:45 -0700

jxplorer (3.3.2+dfsg-2) unstable; urgency=low

  * Team upload.
  * correct LdifExport capitalization of "dn:" (Closes: #721820)
    - Thank you to Maksim Kuleshov for the patch.
  * Bump Standards Version to 3.9.4.
  * Remove deprecated DMUA field.
  * Upload Vcs fields to use canonical URLs.

 -- tony mancill <tmancill@debian.org>  Sun, 08 Sep 2013 09:13:00 -0700

jxplorer (3.3.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Fix icons gamma correction (Closes: #705997).
  * Switch to -project upstream zipfile.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Thu, 04 Apr 2013 01:34:46 +0200

jxplorer (3.3.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Switch source and binary to xz compression.
  * Capitalize Java in extended description (Closes: #640868).
  * Add ca-certificates-java as runtime dependency (Closes: #611721).
  * Depend on default-jre | java6-runtime.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Wed, 08 Aug 2012 01:00:07 +0200

jxplorer (3.2.2~rc1+dfsg-3) unstable; urgency=low

  [ Miguel Landaeta ]
  * Team upload.
  * Make copyright file DEP-5 compliant and fix some lintian warning about it.

  [ James Page ]
  * Fix FTBFS with OpenJDK 7 (LP: #888957) (Closes: #651535):
    - d/rules: Call jh_build with source/target set to 1.5 to ensure
      backwards compatibility and work around Java 7 encoding errors. 

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 13 Dec 2011 19:07:48 -0430

jxplorer (3.2.2~rc1+dfsg-2) unstable; urgency=low

  * Team upload

  [ Gabriele Giacone ]
  * Capitalize LDAP in long description (Closes: #618591).

  [ Torsten Werner ]
  * Switch to default-jdk. (Closes: #640571)

 -- Torsten Werner <twerner@debian.org>  Tue, 06 Sep 2011 21:16:24 +0200

jxplorer (3.2.2~rc1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Switch to "public" package.
  * Bump Standards-Version to 3.9.2 (no changes).
  * Replace 05utflang patch with one from upstream.
  * Add RELEASE.TXT as upstream changelog.
  * Add codeless-jar lintian tag override.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 13 Aug 2011 04:39:00 +0200

jxplorer (3.2.1+dfsg-5) unstable; urgency=low

  * Removed gcj from possible jvms (Closes: #609140).
  * Added package-contains-empty-directory lintian override.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sun, 09 Jan 2011 19:45:25 +0100

jxplorer (3.2.1+dfsg-4) unstable; urgency=low

  * Trusted CAs/servers keystore defaults to /etc/ssl/certs/java/cacerts
    (Closes: 597967).
    + Updated patch 02jkslocation.
    + Removed /usr/share/jxplorer/security.
  * Added patch 05utflang that fixes french translation (Closes: #599557).
  * d/copyright: fixed spacing.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 09 Oct 2010 15:37:05 +0200

jxplorer (3.2.1+dfsg-3) unstable; urgency=low

  * Added empty plugins directory (Closes: #592929).
  * Standards version to 3.9.1.
  * Added patch 04connecthelp to make help window start when pressing
    Help button in connection window.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 14 Aug 2010 14:22:41 +0200

jxplorer (3.2.1+dfsg-2) unstable; urgency=low

  * Added required java-wrappers version (Closes: #590020).
  * Standards version to 3.9.0.
  * Added patch 03defaultdsport to replace default 19389 port with 
    "url.defaultdirectory.port" configuration property (Closes: 589894).

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 23 Jul 2010 21:36:01 +0200

jxplorer (3.2.1+dfsg-1) unstable; urgency=low

   [ Gabriele Giacone ]
   * New upstream release.
   * Added patch 02jkslocation to copy template security directory under
     ~/.jxplorer (Closes: #580846)
   * Merged upstream tarballs into a single one.
   * Removed possible bashism from d/rules. (Closes: #581461)
   * Added extra-license-file lintian override.

   [ Damien Raude-Morvan ]
   * debian/control: Add DMUA flag for Gabriele

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sun, 16 May 2010 11:48:02 +0200

jxplorer (3.2rc2+dfsg-3) unstable; urgency=low

  * Added patch 01paths to fix bookmarks file path and temporary
    directories for media files paths. (Closes: #574181)
  * Removed java-wrappers from build dependencies.
  * Standards-Version to 3.8.4.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Thu, 18 Mar 2010 01:17:22 +0100

jxplorer (3.2rc2+dfsg-2) unstable; urgency=low

  * Added java-wrappers dependency. (Closes: #565730)

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Mon, 18 Jan 2010 18:03:43 +0100

jxplorer (3.2rc2+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #563217)

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 02 Jan 2010 01:18:01 +0100
